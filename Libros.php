<?php
    include 'menu.php';
    include 'conexion.php';
    echo('
        <div align="center" class="container"><br>
    ');

    $sql = "SELECT codigo, titulo, autor, categoria, descarga, calificacion, descripcion FROM libro";
    $i = 0;
   $contador_descargas= 0;
   $puntosPlus= 1;
   $puntosMinus = -1;
    if ($resultado = $conexion->query($sql)) {
        echo("<div class='row' >");
        while($row = $resultado->fetch_array()){
            $codigo= $row['codigo'];
            $titulo = $row['titulo'];
            $autor = $row['autor'];
            $categoria = $row['categoria'];
            $descripcion = $row['descripcion'];
            $calificacion = $row['calificacion'];
            $descargas = $row['descarga'];
            if($categoria=="Educación"){
                echo('
                <div class="card border-dark col-12 col-md-3 p-3 mr-2" style="width:18rem;"> 
                    <img src="imagenes/educacion.jpg" class="pic card-img-down" class="card-img-top" height="200px">
                    <div id="respuesta" class="card-body">
                        <h6 value="<?php echo $codigo; ?>">Título: '.$titulo.'</h6>
                        <h6>Autor: '.$autor.'</h6>
                        <h6>Categoría: '.$categoria.'</h6>
                        <h6>Descripción: '.$descripcion.'</h6>
                        <h6>Calificación: '.$calificacion.' '.'<i class="fas fa-star yellow-text"></i></h6>
                        <h6>Descargas: '.$descargas.'</h6>
                        <a href="descargarPdf.php?codigo='.$codigo.'&descarga='.$descargas.'"  class="btn-floating  btn-do"><i class="fas fa-download fa-2x green-text pr-3"></i></a>&nbsp;
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosPlus.'"  id="megusta" class="btn-floating  btn-up"><i class="fas fa-thumbs-up fa-2x blue-text pr-3"></i></a>
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosMinus.'" id="nomegusta" class="btn-floating  btn-d"><i class="fas fa-thumbs-down fa-2x red-text pr-3""></i></a>
                    </div>
                </div>');
            }
            elseif($categoria=="Literatura"){
                echo('
                <div class="card border-dark col-12 col-md-3 p-3 mr-2" style="width:18rem;"> 
                    <img src="imagenes/literatura.jpg" class="pic card-img-down" class="card-img-top" height="200px">
                    <div class="card-body">
                        <h6>Título: '.$titulo.'</h6>
                        <h6>Autor: '.$autor.'</h6>
                        <h6>Categoría: '.$categoria.'</h6>
                        <h6>Descripción: '.$descripcion.'</h6>
                        <h6>Calificación: '.$calificacion.' '.'<i class="fas fa-star yellow-text"></i></h6>
                        <h6>Descargas: '.$descargas.'</h6>
                        <a href="descargarPdf.php?codigo='.$codigo.'&descarga='.$descargas.'"  class="btn-floating  btn-do"><i class="fas fa-download fa-2x green-text pr-3"></i></a>&nbsp;
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosPlus.'" id="megustaLit" class="btn-floating  btn-up"><i class="fas fa-thumbs-up fa-2x blue-text pr-3"></i></a>
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosMinus.'" id="nomegustaLit" class="btn-floating  btn-d"><i class="fas fa-thumbs-down fa-2x red-text pr-3""></i></a>
                    </div>
                </div>');
            }
            elseif($categoria=="Infantil"){
                echo('
                <div class="card border-dark col-12 col-md-3 p-3 mr-2" style="width:18rem;"> 
                    <img src="imagenes/infantil.png" class="pic card-img-down" class="card-img-top" height="200px">
                    <div class="card-body">
                        <h6>Título: '.$titulo.'</h6>
                        <h6>Autor: '.$autor.'</h6>
                        <h6>Categoría: '.$categoria.'</h6>
                        <h6>Descripción: '.$descripcion.'</h6>
                        <h6>Calificación: '.$calificacion.' '.'<i class="fas fa-star yellow-text"></i></h6>
                        <h6>Descargas: '.$descargas.'</h6>
                        <a href="descargarPdf.php?codigo='.$codigo.'&descarga='.$descargas.'"  class="btn-floating  btn-do"><i class="fas fa-download fa-2x green-text pr-3"></i></a>&nbsp;
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosPlus.'" id="megustaInf" class="btn-floating  btn-up"><i class="fas fa-thumbs-up fa-2x blue-text pr-3"></i></a>
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosMinus.'" id="nomegustaInf" class="btn-floating  btn-d"><i class="fas fa-thumbs-down fa-2x red-text pr-3""></i></a>
                    </div>
                </div>');
            }
            elseif($categoria=="Humor"){
                echo('
                <div class="card border-dark col-12 col-md-3 p-3 mr-2" style="width:18rem;"> 
                    <img src="imagenes/humor.jpg" class="pic card-img-down" class="card-img-top" height="200px">
                    <div class="card-body">
                        <h6>Título: '.$titulo.'</h6>
                        <h6>Autor: '.$autor.'</h6>
                        <h6>Categoría: '.$categoria.'</h6>
                        <h6>Descripción: '.$descripcion.'</h6>
                        <h6>Calificación: '.$calificacion.' '.'<i class="fas fa-star yellow-text"></i></h6>
                        <h6>Descargas: '.$descargas.'</h6>
                        <a href="descargarPdf.php?codigo='.$codigo.'&descarga='.$descargas.'"  class="btn-floating  btn-do"><i class="fas fa-download fa-2x green-text pr-3"></i></a>&nbsp;
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosPlus.'" id="megustaHumor" class="btn-floating  btn-up"><i class="fas fa-thumbs-up fa-2x blue-text pr-3"></i></a>
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosMinus.'" id="nomegustaHumor" class="btn-floating  btn-d"><i class="fas fa-thumbs-down fa-2x red-text pr-3""></i></a>
                    </div>
                </div>');
            }else{
                echo('
                <div class="card border-dark col-12 col-md-3 p-3 mr-2" style="width:18rem;"> 
                    <img src="imagenes/reflexion.png" class="pic card-img-down" class="card-img-top" height="200px">
                    <div class="card-body">
                        <h6>Título: '.$titulo.'</h6>
                        <h6>Autor: '.$autor.'</h6>
                        <h6>Categoría: '.$categoria.'</h6>
                        <h6>Descripción: '.$descripcion.'</h6>
                        <h6>Calificación: '.$calificacion.' '.'<i class="fas fa-star yellow-text"></i></h6>
                        <h6>Descargas: '.$descargas.'</h6>
                        <a href="descargarPdf.php?codigo='.$codigo.'&descarga='.$descargas.'"  class="btn-floating  btn-do"><i class="fas fa-download fa-2x green-text pr-3"></i></a>&nbsp;
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosPlus.'" id="megustaRef" class="btn-floating  btn-up"><i class="fas fa-thumbs-up fa-2x blue-text pr-3"></i></a>
                        <a href="calificarLibro.php?codigo='.$codigo.'&calificacion='.$puntosMinus.'" id="nomegustaHumor" class="btn-floating  btn-d"><i class="fas fa-thumbs-down fa-2x red-text pr-3""></i></a>
                    </div>
                </div>');
            }
                                         
            }
            echo("</div>");
        }   
?>
