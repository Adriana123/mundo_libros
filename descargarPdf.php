<?php
    include "conexion.php";
    if(!$conexion){
        die("Connection failed: " . $conexion->connect_error);
    }else{
        //se obtiene el codigo del libro a descargar
        if(isset($_GET['codigo']) && isset($_GET['descarga'])){
            $codigo = $_GET['codigo'];
            $contador = $_GET['descarga'];

            $sql="SELECT ruta_pdf FROM libro WHERE codigo=$codigo";
            $resultado = $conexion->query($sql);
            while($row = $resultado->fetch_array()){
                $libroPdf = $row['ruta_pdf'];
                if(file_exists($libroPdf)){
                    $contador +=1;
                    // Define headers
                    header("Cache-Control: public");
                    header("Content-Description: File Transfer");
                    header("Content-Disposition: attachment");
                    header("Content-Type: application/pdf");
                    header("Content-Transfer-Encoding: binary");
        
                    // Read the file
                    readfile($libroPdf);
                    //---Actualizar
                    //UPDATE `libro` SET `descarga` = '1' WHERE `libro`.`codigo` = 4; 
                    $sql1 = "UPDATE libro SET descarga='$contador' WHERE libro.codigo=$codigo";
                    //exit;
                    $conexion->close();
                }else{
                    echo 'El archivo no existe<br>';
                }
            }
        }else{echo "No se encontró el libro en la BD";}
    }
?>

        