<?php session_start(); ?>
<?php
    include 'encabezado.php';
    include 'conexion.php';
    // obtiene la cedula enviada como parametro 
    $cedula = $_GET["cedula"];  
    // Se realiza la consulta en la BD de ese registro cuyo id coincida
    $sql = "DELETE FROM escritor WHERE cedula ='$cedula'";

    if($conexion->query($sql) === TRUE) {
        echo('
            <br> <br>
            <div class="container">
                <div class="card">
                <!-- Card image -->
                <div align="center">
                    <img src="imagenes/ok.png" height="150" width="150" >
                </div>
                    <!-- Card content -->
                    <div class="card-body" align="center">
                        <p> El escritor se ha elminado exitosamente </p>
                        <a href="verEscritor.php" class="btn btn-primary">Aceptar</a>
                    </div>
                </div>
            </div>
        ');
    }    
    else {
        echo('
        <br> <br>
        <div class="container">
            <div class="card" >
            <!-- Card image -->
                <div align="center">
                    <img src="imagenes/error.png" height="300" width="300" >
                </div>
                <!-- Card content -->
                <div class="card-body" align="center">
                    <h4 class="card-title"><a>"Error: "'. $sql .'"<br>"'. $conexion->error .'</a></h4>
                    <a href="index.php" class="btn btn-primary">Aceptar</a>
                </div>
            </div>
        </div>
        ');
    }

    include 'footer.php'

?>