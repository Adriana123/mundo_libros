<div id=container>
<?php
    include 'encabezado.php';
    include 'conexion.php';
    session_start();
?>
<div id="container"class="container">
        <br><br>
        <div class="row">
            <div class="col-md-6" align="center">
                <div class="card" style="width: 18rem;">
                    <div class= "card gradient-card-header blue-gradient">
                        <i class="fas fa-unlock-alt fa-2x"></i>
                        <h6>Editar Contraseña<h6>
                    </div>  
                    <div class="card-body">
                        <form id="formEditarEscritor" action="" method="POST">
                            <div class="md-form">
                                <input value="<?php echo $_SESSION['email']; ?>" required type="text" id="email" name="email" class="form-control" disabled>
                            </div>
                            <div class="md-form">
                                <label >Digite una Nueva Contraseña</label>
                                <input type="text" id="contrasenia" name= "contrasenia" class="form-control"><br>
                                <button type="submit" class="btn btn-primary ">Aceptar<i class="fas fa-sign-in-alt"></i></i></button>
                            </div>
                            <div id="respuesta"></div>
                        </form>
                    </div>
                </div>
            </div>
 
            <div class="col-md-6" align="center">
            <div class="card" style="width: 18rem;">
                    <img src="imagenes\escritor.jpg" class="card-img-top">
                <div class="card-body">
                    <a href="formEditarEscritor.php" type="submit" id="submitActualizar" class="btn btn-unique">ACTUALIZAR CONTRASEÑA</a>
                        <br><hr><br>
                        <img src="imagenes\libros.png" class="card-img-top">
                        <div class="card-body">
                        <h4 class="card-title">Libros</h4>
                        <a href="formLibro.php" class="btn btn-warning">AGREGAR LIBRO</a>
                        <a href="verLibro.php" class="btn btn-success">INFORMACION</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php include "footer.php"; ?>
</div>