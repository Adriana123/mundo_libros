<?php
    // Formulario que permite el registro de un Escritor
    include 'encabezado.php';
?>
<div id="mainContainer">
    <div class="container">
        <form id="formEscritor" action="registrarEscritor.php" method="POST">
            <br> <br>
            
            <div align= "center">  
            <i class="fas fa-user-edit fa-6x "></i>
                <h3> Datos del Escritor </h3>
            </div>    
            <!-- Material input -->
            <div class="md-form">
                <input required type="number" maxlength="10" id="cedula" name="cedulaEscritor" class="form-control">
                <label for="cedula">Cédula</label>
            </div>

            <div class="md-form">
                <input required type="text" id="nombre" name="nombreEscritor" class="form-control">
                <label for="nombre">Nombre</label>
            </div>
            <div class="md-form">
                <input required type="text" id="apellido" name="apellidoEscritor" class="form-control">
                <label for="nombre">Apellido</label>
            </div>
            <div class="md-form">
                        <input required type="email" id="email" name="email" class="form-control"placeholder="email@example.com">
                        <label for="usuario">Email</label>
                    </div>

            <div class="md-form">
                <input required type="password" id="contrasenia" name="contraseniaEscritor" class="form-control">
                <label for="contrasenia"> Ingrese su contraseña </label>
            </div>

            <div align="center">
                <button type="submit" class="btn btn-primary btn-rounded btn-lg ">Crear Cuenta</button>
                <a href="formLoginEscritor.php" class="btn btn-warning btn-rounded btn-lg">Regresar </a>
            <div>
            <div id="respuesta"></div>
        </form>
    </div>
</div>
<script
		src="https://code.jquery.com/jquery-3.5.1.min.js"
		integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
		crossorigin="anonymous"></script>
<script src="index.js" ></script>


<?php
    include 'footer.php'
?>