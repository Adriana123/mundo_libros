<?php session_start(); ?>
<?php
    // Formulario para el registro de un libro
    include "encabezado.php";
?>
<style>
    .container{
        width:60%
    }

    .md-form{
        width:55%
        
    }
</style>

<div class="container">
    <form action="" method="POST" enctype="multipart/form-data" id="formLibro">
        <br><br>
        <div class="card ">
            <div class= "card gradient-card-header peach-gradient">
                <div align= "center"> 
                    <i class="fas fa-book fa-4x rounded-circle"></i>
                    <h3 class="card-header-title"> Datos del Libro </h3>  
                </div>
            </div>  
            <!-- Material input -->
            <div align= "center"> 
                <div class="md-form">
                    <input required type="text" id="titulo" name="titulo" class="form-control">
                    <label>Título</label>
                </div>

                <div class="md-form">
                    <input value="<?php echo $_SESSION['email']; ?>" required type="text"id="autor" name="autor" class="form-control" disabled>
                    <i class="fas fa-user prefix"></i><br>
                </div>
                <div class="md-form">
                    <i class="fas fa-pencil-alt prefix"></i>
                    <textarea id="descripcion" name="descripcion" class="md-textarea form-control" rows="3"></textarea>
                    <label>Descripción:</label>
                </div>
                <div class="md-form">
                    <label class="mdb-main-label">Categoría </label>
                    <select id="categoria" name="categoria" class="mdb-select md-form colorful-select dropdown-warning">
                        <option value="Educación">Educación</option>
                        <option value="Infantil">Infantil</option>
                        <option value="Literatura">Literatura</option>
                        <option value="Humor">Humor</option>
                        <option value="Reflexión">Reflexión</option>
                    </select> 
                </div>
                <div class="md-form">
                    <label >Selecione el libro en formato pdf:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-file-pdf fa-2x"></i><br>
                    <div class="row">
                        <div class="col">
                            <input required type="file" id="filePdf" name="filePdf" class="form-control">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-orange">Agregar&nbsp;<i class="fas fa-plus fa-2x"></i></i></button>
                <a href=homeEscritor.php class="btn btn-success btn-rounded">Regresar&nbsp;<i class="fas fa-arrow-left fa-2x"></i></a>
                <div id="respuesta"></div>
            </div>
        </div>       
    </form>
</div>


<?php
    include 'footer.php'
?>