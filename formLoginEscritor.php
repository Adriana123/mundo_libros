<style>
    .container{
        width:60%
    }

    .md-form{
        width:55%
        
    }
</style>
<div id="mainContainer">
    <?php
    // Formulario para autenticacion del escritor
    include 'menu.php';
    ?>  
    <div class="container">
        <form id="formLoginEscritor" action="" method="POST">
            <br> <br>
            <div class="card ">
                <div class= "card gradient-card-header blue-gradient">
                    <div align= "center"> 
                    <i class="fas fa-user-edit fa-4x "></i>
                        <h3 class="card-header-title"> Iniciar Sesión </h3>  
                    </div>
                </div>  
                <!-- Material input -->
                <div align= "center"> 
                    <div class="md-form">
                        <input required type="email" id="email" name="usuarioEscritor" class="form-control" placeholder="email@example.com">
                        <label for="usuario">Email</label>
                    </div>
                    <div class="md-form">
                        <input required type="password" id="contraseniaEscritor" name="contraseniaEscritor" class="form-control" >
                        <label for="contrasenia">Contraseña</label><br>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-rounded btn-lg">Aceptar</button>
                            <a href="formEscritor.php"class="btn btn-success btn-roundedbtn-lg" >Registrarse</a>
                        </div>
                    </div>
                </div>
            </div>   
        </form>
    </div>
</div>

<?php
    include 'footer.php'
?>