<?php include "encabezado.php";?>
<?php session_start(); ?>
<br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-6" align="center">
                <div class="card" style="width: 18rem;">
                    <div class="avatar mx-auto white">
                        <img src="imagenes\escritor.jpg" class="card-img-top">
                    </div>
                    <div class="card-body">
                      <h4 class="card-title">Escritores</h4>
                      <a href="verEscritor.php" class="btn btn-primary">VISUALIZAR</a>
                    </div>
                </div>
            </div>
 
            <div class="col-md-6" align="center">
                <div class="card" style="width: 18rem;">
                    <img src="imagenes\fondo.jpg" class="card-img-top">
                    <div class="card-body">
                      <h4 class="card-title">Libros</h4>
                      <a href="verLibrosAdmin.php" class="btn btn-primary">VISUALIZAR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include "footer.php";?>   


