<?php include "encabezado.php";?>
<?php session_start(); ?>
<br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-6" align="center">
                <div class="card" style="width: 18rem;">
                <i class="fas fa-user-edit fa-6x card-img-top"></i> 
                    <div class="card-body">
                      <h5 class="card-title"><?php echo "Bienvenido " . $_SESSION['email']; ?></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6" align="center">
                <div class="card" style="width: 18rem;">
                    <img src="imagenes\escritor.jpg" class="card-img-top">
                <div class="card-body">
                    <a href="formEditarEscritor.php" type="submit" id="submitActualizar" class="btn btn-unique">ACTUALIZAR CONTRASEÑA</a>
                        <br><hr><br>
                        <img src="imagenes\libros.png" class="card-img-top">
                        <div class="card-body">
                        <h4 class="card-title">Libros</h4>
                        <a href="formLibro.php" class="btn btn-warning">AGREGAR LIBRO</a>
                        <a href="verLibro.php" class="btn btn-success">INFORMACIÓN DEL LIBRO</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
