// Registrar Escritor
$('#formEscritor').on('submit', (e) => {
    e.preventDefault();
    var info = new FormData($('#formEscritor')[0]);
    $.ajax({
        url: "registrarEscritor.php",
        type: "POST",
        data: info,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success:function (datos){
            $("#respuesta").html(datos);
        },
        error:function(){
            alert("No se ha podido obtener la información");
        },
    });
});
// Autenticar escritor
$('#formLoginEscritor').on('submit', (e) =>{
    e.preventDefault();
    var info = new FormData($('#formLoginEscritor')[0]);
    $.ajax({
        url: "loginEscritor.php",
        type: "POST",
        data: info,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success:function (datos){
            $("#mainContainer").html(datos);
        },
        error:function(datos){
            alert("No se ha podido obtener la información");
            alert(datos);
        },
        beforeSend: function(){
            alert("Enviando");
        } 
    });
});
// Autenticar administrador
$('#formLoginAdmin').on('submit', (e) =>{
    e.preventDefault();
    var info = new FormData($('#formLoginAdmin')[0]);
    $.ajax({
        url: "loginAdmin.php",
        type: "POST",
        data: info,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success:function (datos){
            $("#mainContainer").html(datos);
        },
        error:function(){
            alert("No se ha podido obtener la información");
        },
    });
});
// Registrar Libro
$('#formLibro').on('submit', (e) => {
    e.preventDefault();
    var info = new FormData($('#formLibro')[0]);
    $.ajax({
        url: "registrarLibro.php",
        type: "POST",
        enctype: 'multipart/form-data',
        data: info,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success:function (datos){
            alert("entro");
            $("#respuesta").html(datos);
        },
        error:function(){
            alert("No se ha podido obtener la información");
        },
        beforeSend: function(){
            alert("Enviando");
        } 
    });
});
//Editar contraseña del escritor
$('#formEditarEscritor').on('submit', (e) =>{
    e.preventDefault();
    var info = new FormData($('#formEditarEscritor')[0]);
    $.ajax({
        url: "editarEscritor.php",
        type: "POST",
        data: info,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success:function (datos){
            $("#respuesta").html(datos);
        },
        error:function(datos){
            alert("No se ha podido obtener la información");
            alert(datos);
        },
        beforeSend: function(){
            alert("Enviando");
        } 
    });
});
