<?php
    include 'conexion.php';
    if(!$conexion) {
        die("Connection failed: " . $conexion->connect_error);
    }
    else{
        //---------------------------------------------AUTENTICACIÓN ADMINISTRADOR--------------------------------------
        if(isset($_POST['usuarioAdmin']) && isset($_POST['contraseniaAdmin'])){
                
            //Se reciben los datos 
            //mysql_real_escape_string — Escapa caracteres especiales en una cadena para su uso en una sentencia SQL
            $contraseniaAd = mysqli_real_escape_string($conexion, $_POST['contraseniaAdmin']);
            $usuarioAdmin = mysqli_real_escape_string($conexion, $_POST['usuarioAdmin']);
                
            $contraAdmin = md5($contraseniaAd); //codificar contraseña

            // consulta para buscar el usuario administrador en la BD
            $sql = "SELECT email, contrasenia FROM administrador WHERE email=? AND contrasenia=?";
            //---------------------------------------------------------------
            echo 'entro<br>';
            
            session_start();
            $sqlSesion = $conexion->prepare($sql);
            $sqlSesion->bind_param('ss', $usuarioAdmin, $contraAdmin);
            $sqlSesion->execute();
            $resultado = $sqlSesion->get_result();
            $Filas = $resultado->num_rows;
            echo ".$Filas";
            // Se verifica si los datos corresponden al administrador de la bd que ya se encuentra registrado
            if ($Filas > 0) {
                echo "encontro una coincidencia";
                while ($fila = mysqli_fetch_array($resultado)) { 
                    if ($usuarioAdmin == $fila['email'] && $contraAdmin == $fila['contrasenia']) {
                        $_SESSION['email'] = $fila['email']; // Se crea sesión con el email del administrador
                        echo 'ok';
                        $conexion->close();
                        header('location:homeAdmin.php'); // se redirije a homeAdmin.php
                    }
                }  
                    
            }
            else{
                header('location:index.php');
                echo "Error en la autenticacion";
            }
            
        }
        else{
            echo "Campos vacios<br>";
        }
        
    }
?>