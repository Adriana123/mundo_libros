<?php
    include 'conexion.php';

    if(!$conexion) {
        die("Connection failed: " . $conexion->connect_error);
    }
    else {
        //---------------------------------------------AUTENTICACIÓN ESCRITOR--------------------------------------
        if(isset($_POST['usuarioEscritor']) && isset($_POST['contraseniaEscritor'])){
            //Se reciben los datos 
            //mysql_real_escape_string — Escapa caracteres especiales en una cadena para su uso en una sentencia SQL
            $contraseniaEs = mysqli_real_escape_string($conexion, $_POST['contraseniaEscritor']);
            $usuarioEscritor = mysqli_real_escape_string($conexion, $_POST['usuarioEscritor']);
            
            $contraEscritor = md5($contraseniaEs); //codificar contraseña

            // consulta para buscar el usuario escritor en la BD
            $sql = "SELECT contrasenia, email FROM escritor WHERE contrasenia=? AND email=?";
            //---------------------------------------------------------------
            session_start();
            $sqlSesion = $conexion->prepare($sql);
            $sqlSesion->bind_param('ss',$contraEscritor, $usuarioEscritor);
            $sqlSesion->execute();
            $resultado = $sqlSesion->get_result();
            $Filas = $resultado->num_rows;

            // Se verifica si los datos corresponden al escritor de la bd que ya se encuentra registrado
            if ($Filas > 0) {
                echo "encontro una coincidencia";
                while ($fila = mysqli_fetch_array($resultado)) { 
                    if ($usuarioEscritor == $fila['email'] && $contraEscritor == $fila['contrasenia']) {
                        $_SESSION['email'] = $fila['email']; // Se crea sesión con el cemail del escritor
                        echo 'ok';
                        $conexion->close();
                        header('location:homeEscritor.php'); // se redirije a homeEscritor.php
                    }
                }  
                
            }
            echo "<p>Error en la autenticacion <p>";
        }else { echo "campos vacios";}             
        
    }
    
?>