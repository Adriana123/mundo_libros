<?php
    // Registrar un escritor en la base de datos
    include 'conexion.php';

    //---Verifica si hay errores al conectarse con la BD
    if($conexion->connect_error) {
        die("Connection failed: " . $conexion->connect_error);
    }else {
        if(isset($_POST["contraseniaEscritor"]) && isset($_POST["cedulaEscritor"]) && isset($_POST["nombreEscritor"]) && isset($_POST["apellidoEscritor"]) && isset($_POST["email"])){
            // se obtienen los datos
            $contrasenia_aux = $_POST["contraseniaEscritor"];
            $cedula = $_POST["cedulaEscritor"];
            $nombre_aux = strtolower($_POST["nombreEscritor"]);
            $apellido_aux = strtolower($_POST["apellidoEscritor"]);
            $nombre=ucfirst($nombre_aux);
            $apellido=ucfirst($apellido_aux);
            $email = $_POST["email"]; 

            //----------------------------------------------------------------
            $contrasenia = md5($contrasenia_aux);  // Cifrado de contraseña 
            //----------------------------------------------------------------
            $errores = 0;
            if (!is_numeric($cedula) && strlen($cedula) > 10){  // validar cedula
                $mostrar_error= "La contraseña no es valida"."<br>";
                $errores += 1;
            } 
            // ctype_alpha() Verifica si todos los caracteres en la string entregada, son alfabéticos
            if (!ctype_alpha($nombre)){
                $mostrar_error = "El nombre no es valido"."<br>";
                $errores += 1;
            }
            if (!ctype_alpha($apellido)){
                $mostrar_error= "El apellido no es valido";
                $errores += 1;
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $mostrar_error= "El email no es valido"."<br>";
                $errores += 1;
            }
            if ($errores == 0){
                // ----Consulta para insertar un escritor---
                $sql="INSERT INTO escritor (contrasenia, cedula, nombre, apellido, email)
                VALUES (?, ?, ?, ?, ?)";

                // Sentencia preparada
                $sentencia = $conexion->prepare($sql);
                $sentencia->bind_param('sssss', $contrasenia, $cedula, $nombre, $apellido, $email);
                if(!$sentencia->execute()) {
                    //echo "Error: " . $sql . "<br>" . $conexion->error;
                
                }
                else{
                    echo "<p>Registro exitoso</p>";
                }

            }else{
                echo ".$mostrar_error";
            }
            

        }
        $conexion->close();
    }
?>