<?php
session_start();
    // Registrar un libro en la base de datos
    include 'conexion.php';

    //---Verifica si hay errores al conectarse con la BD
    if($conexion->connect_error) {
        die("Connection failed: " . $conexion->connect_error);
    }else{
        if(isset($_POST["titulo"]) && isset($_POST["descripcion"]) && isset($_POST["categoria"]) && isset($_FILES["filePdf"])){
            // se obtienen los datos
            $titulo = $_POST["titulo"];
            $autor = $_SESSION['email'];
            $descripcion = $_POST["descripcion"];
            $categoria = $_POST["categoria"];
            //
            $archivo = $_FILES['filePdf'];
            $nombrepdf= $_FILES['filePdf']['name'];
            $tipo=$_FILES['filePdf']['type'];
            $ruta = $_FILES['filePdf']['tmp_name'];
            $destino= "libros/".$nombrepdf;
            //  valida que sea del formato pdf
            if($tipo == 'application/pdf'){
                if (copy($ruta,$destino)){
                    $sql="INSERT INTO libro (titulo, autor, categoria, ruta_pdf, descripcion)
                    VALUES (?, ?, ?, ?, ?)";

                // Sentencia preparada
                $sentencia = $conexion->prepare($sql);
                $sentencia->bind_param('sssss', $titulo, $autor, $categoria, $destino, $descripcion);
                if(!$sentencia->execute()) {
                    echo "Error: " . $sql . "<br>" . $conexion->error;
                
                }
                else{
                    echo "<p>¡Registro exitoso!</p>";
                }
                }
                else{
                    echo"Error";
                }

            }
            else{
                echo "$tipo.<br>";
                echo "No es un pdf.<br>";
            }
        }   
    }   
?>