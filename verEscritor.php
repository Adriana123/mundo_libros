<?php
    include 'encabezado.php';
    include 'conexion.php';
    session_start();
    echo('
        <div align="center" class="container"> 
    ');

    $sql = "SELECT cedula, nombre, apellido, email FROM escritor";
    $i = 0;

    if ($resultado = $conexion->query($sql)) {
        echo('
            <table class="table">
                <thead class="aqua-gradient white-text">
                    <tr><th><i class="fas fa-user fa-3x "></i></th>
                        <th scope="col"><h6>Cedula</h6></th>
                        <th scope="col"><h6>Nombre</h6></th>
                        <th scope="col"><h6>Apellido</h6></th>
                        <th scope="col"><h6>Email</h6></th>
                        <th scope="col"><h6>Eliminar</h6></th>
                    </tr>
                </thead>
                
        ');

        while($row = $resultado->fetch_array()){ 
            $i = $i + 1;
            $cedula = $row['cedula'];
            $nombre = $row['nombre'];
            $apellido = $row['apellido'];   
            $email = $row['email']; 
            echo('
                <tbody>
                    <tr>
                        <th scope="row">'.$i.'</th>
                        <td>'.$cedula.'</td>
                        <td>'.$nombre.'</td>
                        <td>'.$apellido.'</td>
                        <td>'.$email.'</td>
                        <td> 
                        <a href="eliminarEscritor.php?cedula='.$cedula.' "><i class="fas fa-minus-circle fa-2x red-text pr-3" aria-hidden="true"></i></a>  
                        </td>
                    </tr>
            ');                        
        }

        echo('   
                </tbody>
            </table>
            <a href=homeAdmin.php class="btn btn-primary btn-rounded">REGRESAR</a>
        </div>
        ');   
    }
    $conexion->close();
?>
