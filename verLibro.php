<?php
    include 'encabezado.php';
    include 'conexion.php';
    session_start();
    echo('
        <div align="center" class="container"> 
    ');

    $sql = "SELECT titulo, categoria, descarga, calificacion FROM libro";
    $i = 0;

    if ($resultado = $conexion->query($sql)) {
        echo('
            <br><br>
            <table class="table">
                <thead class= "peach-gradient">
                    <tr><th><i class="fas fa-book fa-3x "></i></th>
                        <th scope="col"><h6>Título</h6></th>
                        <th scope="col"><h6>Categoría</h6></th>
                        <th scope="col"><h6>Descarga</h6></th>
                        <th scope="col"><h6>Calificación</h6></th>
                        <th scope="col"><h6>Eliminar</h6></th>
                    </tr>
                </thead>
                
        ');

        while($row = $resultado->fetch_array()){ 
            $i = $i + 1;
            $titulo = $row['titulo'];
            $categoria = $row['categoria'];
            $descarga = $row['descarga'];   
            $calificacion = $row['calificacion']; 
            echo('
                <tbody>
                    <tr>
                        <th scope="row">'.$i.'</th>
                        <td>'.$titulo.'</td>
                        <td>'.$categoria.'</td>
                        <td>'.$descarga.'</td>
                        <td>'.$calificacion.'</td>
                        <td> 
                        <a href= "eliminarEscritor.php?cedula='.$titulo.' "><i class="fas fa-minus-circle fa-2x red-text pr-3" aria-hidden="true"></i></a>  
                        </td>
                    </tr>
            ');                        
        }

        echo('   
                </tbody>
            </table>
            <a href=homeEscritor.php class="btn btn-primary btn-rounded">REGRESAR</a>
        </div>
        ');   
    }
   
?>
